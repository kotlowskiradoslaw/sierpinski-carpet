using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Dywan_Sierpińskiego
{
    static class Functions
    {
        public static void Draw_line(Point start, Point end, int time)
        {
            Form1.graphics.DrawLine(Form1.pen, start, end);
            Wait(time);
        }

        public static void Draw_sierpinski()
        {
            int biggest_square_size = Get_square_size(Screen.PrimaryScreen.WorkingArea);
            int iterations_number = Get_iterations_number(biggest_square_size)+1;
            Point start_point = Get_start_point(Screen.PrimaryScreen.WorkingArea, biggest_square_size);

            Square_Sierpinski.biggest_square_size = biggest_square_size;

            Square_Sierpinski[][] squares = new Square_Sierpinski[((int)Math.Pow(8, iterations_number) - 1) / 7][];
            squares[0] = new Square_Sierpinski[]{ new Square_Sierpinski(start_point) };
            for (int i = 1; i < iterations_number; i++) squares[i] = Get_all_children(squares[i - 1]);

            Paint_squares(squares);
        }

        private static Square_Sierpinski[] Get_all_children(Square_Sierpinski[] squares)
        {
            Square_Sierpinski[][] children = new Square_Sierpinski[squares.Length][];
            for (int i = 0; i < children.Length; i++)
            {
                children[i] = squares[i].Create_children();
            }
            return children.SelectMany(item => item).Distinct().ToArray();
        }

        private static int Get_iterations_number(int biggest_square_size)
        {
            return (int)Math.Round(Math.Log(biggest_square_size, 3));
        }

        private static int Get_square_size(Rectangle w_area)
        {
            return Math.Min(w_area.Width, w_area.Height);
        }

        private static Point Get_start_point(Rectangle w_area, int square_size)
        {
            return new Point(
                (w_area.Width - square_size) / 2,
                (w_area.Height - square_size) / 2
            );
        }

        private static void Paint_squares(object squares)
        {
            if (squares is Square_Sierpinski sierpinski)
            {
                if (sierpinski.filled && !sierpinski.has_filled_ancestor) sierpinski.Paint();
            }
            else if (squares is null) return;
            else
            {
                foreach (var square in (Array)squares)
                {
                    Paint_squares(square);
                }
            }
        }

        private static void Wait(int time)
        {
            System.Threading.Thread.Sleep(time);
        }
    }
}
