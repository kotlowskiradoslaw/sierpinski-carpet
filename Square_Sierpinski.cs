using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Dywan_Sierpińskiego
{
    class Square_Sierpinski
    {
        public static int biggest_square_size;

        public int size;
        public int iteration;
        public bool filled;
        public Point left_top_point;
        public Point placement;
        public bool has_filled_ancestor;

        public Square_Sierpinski(Point start_point)
        {
            placement = new Point();
            iteration = 0;
            filled = false;
            has_filled_ancestor = false;
            size = biggest_square_size;
            left_top_point = start_point + new Size(size * placement.X, size * placement.Y);

        }

        public Square_Sierpinski(Square_Sierpinski parent_square, Point pl)
        {
            placement = pl;

            iteration = parent_square.iteration + 1;
            filled = placement == new Point(1, 1) && !parent_square.filled;
            has_filled_ancestor = parent_square.has_filled_ancestor || parent_square.filled;
            size = (int)Math.Round(biggest_square_size / Math.Pow(3, iteration));
            left_top_point = parent_square.left_top_point + new Size(size * placement.X, size * placement.Y);
        }

        public void Paint()
        {
            for (int height = 0; height <= size; height++)
            {
                Point start = Point.Add(left_top_point, new Size(0, height));
                Point end = Point.Add(left_top_point, new Size(size, height));
                Functions.Draw_line(start, end, 0);
            }
        }

        public Square_Sierpinski[] Create_children()
        {
            Square_Sierpinski[] children = new Square_Sierpinski[9];

            for (int i = 0; i < 9; i++)
            {
                children[i] = new Square_Sierpinski(this, new Point(i % 3, i / 3));
            }

            return children;
        }
    }
}
