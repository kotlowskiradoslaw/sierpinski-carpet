using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dywan_Sierpińskiego
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            graphics = CreateGraphics();
        }

        public static Pen pen = new Pen(Color.Black, 1);
        public static Graphics graphics;

        private void button_draw_Click(object sender, EventArgs e)
        {
            Functions.Draw_sierpinski();
        }

        private void button_erase_Click(object sender, EventArgs e)
        {
            if (graphics != null)
            {
                graphics.Clear(Color.White);
            }
        }
    }
}
